#!/usr/bin/env bash

printf "{ \"accessKeyId\": \"%s\", \"secretAccessKey\": \"%s\", \"region\": \"us-east-1\" }\n" "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" >> credentials.json
printf "module.exports = { credentials: \"credentials.json\", bucketName: \"docs.textile.io\", patterns: [\"css/*.css\", \"assets/**/*\", \"fonts/*\", \"img/*\", \"js/*.js\", \"search/*.json\", \"*.html\", \"**/*.html\", \"sitemap.xml\"] }" >> aws-upload.conf.js
