#!/usr/bin/env bash

rm -rf node_modules
rm -rf vendor
rm -rf css
rm -rf js/*.min.js
