Mill is the official, lightweight Python client for interacting with Textile's REST API.

Installation
============

Mill is supported on Python 2.7, 3.3, 3.4, 3.5 and 3.6. The recommended way to install Mill is via [pip](https://pypi.python.org/pypi/pip).

```bash
pip install mill
```

To install the latest development version of Mill run the following instead:

```bash
pip install --upgrade git+git://gitlab.com/textileio/mill-py.git
```

For instructions on installing Python and pip see "The Hitchhiker's Guide to Python" [Installation Guides](http://docs.python-guide.org/en/latest/starting/installation/).

Quickstart
==========

Authentication
--------------

Textile's REST API uses scoped [JSON Web Tokens (JWT)](https://jwt.io/) to authenticate with external clients. For example, whenever a request is made by the Mill Python client, one of these tokens is passed along via [HTTP Basic Auth](https://en.wikipedia.org/wiki/Basic_access_authentication), identifying the requester and stating what specific permissions they have.

> **note**
>
> We take your users' privacy very seriously. So, with the exception of registering your client, communications between the Mill client and Textile's REST API are authenticated with user-scoped tokens.

In order to use the Mill Python client, you'll need to pass a `` `client_id ``\` and `` `client_secret ``\` pair to the Mill `Client` object. These are only used to register your client, at which point they'll be exchanged for scoped tokens which are used internally by the Mill `Client`. Please check out Textile's [documentation](http://docs.textile.io/overview/accounts-security/) to learn how to create an account and get your client credentials.

Once you have your credentials in hand, instantiation, authenticating, and interacting with the Mill `Client` is straightforward:

```python
from datetime import datetime, timedelta
from mill import Client
client = Client(client_id="uuid",
                client_secret="shhh",
                bundle="com.bundle.id",
                api_url="https://api.textile.io")
```

With the `client` instance you can now interact with Textile's REST API:

```python
# Get health of the api endpoint
client.get_health()
# Request all features for the given app (default for the past day)
ndjson = client.request_features()
# Alternatively, you can specify specific query parameters
ndjson = client.request_features(lookback=timedelta(days=2))
# Should be equivalent to the above query
ndjson = client.request_features(start=datetime.now() - timedelta(days=2))
# Ignore everything in the last day
ndjson = client.request_features(start=datetime.now() - timedelta(days=2),
                                 end=datetime.now() - timedelta(days=1))
# Query for a specific feature type, which can include any/all of:
# ["system", "states", "events", "arrivals", "departures", "places", "trips", "tags", "cycles", "models"]
ndjson = client.request_features(types=["arrivals", "departures"])
```

The above queries all return a generator over the features (so the calls will return almost instantly). You can then iterate over the features, convert them to a list, etc. For large requests, you can also stream the features directly to a file using the [ndjson](http://ndjson.org) format:

```python
# This method also takes all the same query parameters as `request_features`
params = {...}
client.download_features("output.ndjson", **params)
# Alternatively, you can specify your own file-like object to write to
with open("output.ndjson", "w+") as f:  # remember to use `with` to automatically close and flush files
    client.download_features(f)
```

Mill then makes it easy to work with your preferred SciPy libraries and tools:

```python
# Import pandas or other data science libraries
import pandas as pd
from pandas.io.json import json_normalize
df = json_normalize(ndjson)
# Now start the data-science!
df.head()
df.arrival_time_normality_score.plot()
```

Discussion and Support
======================

Real-time chat can be conducted via Textile's [developer slack channel](https://textile-public.slack.com).

Please file bugs and feature requests as issues on ... after first searching to ensure a similar issue was not already filed. If such an issue already exists please give it a thumbs up reaction. Comments to issues containing additional information are certainly welcome.

> **note**
>
> This project is released with a [Contributor Code of Conduct](./CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

Documentation
=============

Please see Mill's [documentation](http://docs.textile.io/clients/python/) for more examples of what you can do with Mill.
