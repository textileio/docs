To install Textile in your mobile app, you will need to follow the instructions for the Loom SDK. Once installed, the SDK will begin to make data available in your Dashboard and through the Textile APIs. 

* Get started by installing the [Loom iOS SDK](/sdk/ios.md).

_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).