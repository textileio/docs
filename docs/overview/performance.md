## Performance

SDKs are often an important resource for mobile developers. However, with every new library, it is important to be sure that there is no degradation to the performance of ones own app or impact to the end user's experience. In this section, we'll highlight a few of our performance benchmarks to give you some piece of mind.

> **Benchmarks**
>
> All of these data have been collected from various experiments that may not match your app's use of the Loom SDK exactly. If you'd like to discuss custom performance benchmarks please contact us at [contact@textile.io](mailto:contact@textile.io).

## Battery Life

Use of the Loom SDK in normal phone conditions has negligible impact on battery life. If user location is enabled, the Loom SDK still increase an app's battery impact by only 1% in most circumstances and never more than 3%. If your app is not a general consumer application and has atypical use patterns that you think may impact performance, we're happy to discuss optimization options.

## CPU and Memory

The Loom SDK's impact on your app's CPU and memory requirements is in generally very small. Average CPU usage is less than 1%, with occasional sub-second usage of around 25% on the A9 chipset. Those spikes will increase depending on your usage of the Loom SDK, primarily how frequently you utilize Custom Events.

## Network Usage 

The Loom SDK is designed to minimize battery impact from network usage and data consumption over cellular connections.

## Permissions 

The Loom SDK doesn't require any permissions beyond the ability to run Background Tasks. Read more in the Loom integration instructions for [iOS](/sdk/ios/) or [Android](/sdk/android/).

## Crash Rates

It is critical that any mobile SDK have little to no impact on an apps crash rate. We pride ourselves on an extremely low crash rate. We are still collecting data to give a precise number, [let's talk](mailto:contact@set.gl) if you need those numbers earlier.

_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).