
## Authentication

In this guide, we'll walk through some of the definitions used in Textile, take a look at how we handle security and privacy, and _finally_, how to [get your client credentials](#get-your-client-credentials)!

## Definitions

Let's just quickly hit the basics:

* A **Developer**... That's you!
* A **Client** corresponds to one mobile app. You may create however many clients you desire.
    * Each client will have its own ```clientId``` and ```clientSecret``` pair.
    * Each client will have its own set of _users_.
* A **User** is one of your mobile app users.

Okay. Pretty straightforward right? Onward...

## Security Overview

The Textile REST API uses scoped [JSON Web Tokens (JWTs)](https://jwt.io/) to authenticate with the Loom SDK and external integrations (like IFTTT and Alexa). For example, whenever a request is made by the SDK from within your app, one of these tokens is passed along via [HTTP Basic Auth](https://en.wikipedia.org/wiki/Basic_access_authentication), identifying the requester and stating what specific permissions they have.

> **Privacy**
>
> We take your users' privacy very seriously. So, with the exception of registering your client, communications between the Loom SDK and our REST API are authenticated with user-scoped tokens. This keeps each of your users nicely siloed and secure.

## Get your Client Credentials

In order to run the Loom SDK, you'll need to pass a ```clientId``` and ```clientSecret``` pair to [authorize](/sdk/ios/#authorize-the-loom-sdk). These are only used to authorize your client, at which point they'll be exchanged for scoped tokens which are used internally by the Loom SDK. **Don't share your client credentials!**

### 1. Create your Textile Account

Navigate to our [Developer Portal](https://mill.textile.io) and create a new account.

### 2. Verify your Email Address

After you create your account, you'll be able to create clients, but client registration from the Loom SDK will fail unless you've verified your email address. To do this, just click the "Verify Email" button in the email that was sent to you.

### 3. Create a New Client for your Mobile App

Click the big green "Create your first client" button.

![](/img/dahsboard-create-client.png)

Enter your client details into the form, and click "Create Client".

![](/img/dashboard-enter-details.png)

### 4. Find your Client Credentials

Click on your newly created client in the clients list. The "Overview" section contains your client details, including the ```clientId``` and ```clientSecret``` pair.

## Creating APN Keys

Next, you'll need to add your [APN keys from Apple](https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/APNSOverview.html#//apple_ref/doc/uid/TP40008194-CH8-SW1) to your [Dashboard](https://mill.textile.io). Textile uses the APN to send Silent Push requests in order to process data in the background.

1. Visit the [Apple Developer Account](https://developer.apple.com/account/).
2. Click on *Certificates, Identifiers & Profiles*.
3. Go to *Keys* in the left-hand menu.
4. Click the "+" button in the upper right-hand corner.
5. Create a descriptive name for you new Key.
6. Check the box under *Enable* to the left of *APNs*.
7. Hit *Continue*.
8. In this page, copy the value next to *Key ID*, this is the *APN Service Key ID* you'll need to drop into your Dashboard.
9. Click *Download*.
10. Finally, you'll copy the contents of the file you downloaded. The contents of that file are what go into *APN Service Key*.

## Getting your Team ID

1. Visit the [Apple Developer Account](https://developer.apple.com/account/).
2. In the menu at the top, click *Account*.
3. In the menu on the left, click *Membership*.
4. Scroll down and copy the value in the row, *Team ID*

## Add Your Keys to the Textile Dashboard

We require the following information:

* Team ID
* APN Service Key ID
* APN Service Key

You can copy and paste your keys into Textile by logging into your Mill Dashboard, going to Settings and scrolling down to *APN Credentials*.

![APN Setup](/img/apn-keys.png)

_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).