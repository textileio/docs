# Android Coming Soon!

_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).