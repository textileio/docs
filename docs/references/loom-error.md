```swift
enum LoomError: Error
```
The errors that may be returned to your app through various Loom APIs.
_____________________________

#### Error Types

```swift
case authorizationAlreadyInProcess
```
Loom was in the middle of the authorization process when an additional call to authorize Loom was received.

```swift
case sdkAlreadyRunning
```
Loom was already running when an additional request to launch Loom was received.

```swift
case sdkNotInitialized
```
One of Loom's APIs that require Loom to be authorized was called before Loom was authorized.

```swift
case invalidAppToken
```
The client id or secret used to authorize Loom was incorrect.

```swift
case missingBundleId
```
The app authorizing Loom doesn't have a valid bundle identifier.

```swift
case internalError
```
An error internal to Loom was triggered.

```swift
case jsonDecodingError
```
There was an error parsing data received from Textile's web APIs.

```swift
case error(error: Error)
```
Wraps any other iOS error that may have been triggered.

_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).
