```swift
class PermissionsHelper
```
Provides helper functions for prompting the user for location and motion data permission without having to interact with `CoreLocation` or `CoreMotion` APIs directly.
_____________________________

#### Prompting for Location Permission

```swift
static func requestLocationWhileInUse()
```
Prompt the user for permission to access location data while the app is in use. Uses the privacy usage descriptions from your app's `Info.plist` file.

```swift
static func requestLocationAlways()
```
Prompt the user for permission to access location data while the app is in use and while in the background. Uses the privacy usage descriptions from your app's `Info.plist` file.

#### Prompting for Motion Data Access

```swift
static func requestMotionActivityAccess()
```
Prompt the user for access to motion activity data. Also enables Loom to query for motion data when learning user behaviors and generating feature data.

_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).
