```swift
extension CMMotionActivityManager
```

This extension to `CMMotionActivityManager` provides methods to prompt the user for motion data access and enable Loom access to that data.
_____________________________

#### Enabling Motion Activity Access

```swift
static func requestPermission()
```
If your app has not access motion data prior to integrating Loom, displays a prompt to the user asking for access to motion activity data, and enable Loom access to motion activity data. For versions of iOS less than 11, the value of `permissionGranted` will be `true` after this is called no matter if the user allowed or denied the request. In iOS 11 and higher, `permissionGranted` will accurately reflect the user's choice.

```swift
static func enableMotionActivity()
```
Enable Loom access to motion activity data if you're app has accessed motion data previous to integrating Loom. For versions of iOS less than 11, the value of `permissionGranted` will be `true` after this is called. In iOS 11 and higher, `permissionGranted` will accurately reflect whether the user allowed or denied motion data access.

#### Querying Current Motion Data Permissions

```swift
static var permissionGranted: Bool
```
Returns a `Bool` indicating if the user has granted access to motion activity data. On iOS 11 and higher, this uses iOS's `CMMotionActivityManager.authorizationStatus()`. In earlier versions of iOS, the value is based on whether or not the `requestPermission()` and/or `enableMotionActivity()` methods in this extension have been called.

_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).
