```swift
class Loom
```
This is the main object in the Loom SDK. It contains the entire Loom runtime and provides most of the APIs that iOS developers will interact with.
_____________________________

#### Authorizing Loom

```swift
static func authorize(with configuration: Credentials, completion: @escaping (LoomError?) -> Void)
```
Authorize Loom with your client `Credentials`. The `completion` handler will be called with a `nil` error if Loom was successfully authorized, or a `LoomError` if there was a problem authorizing Loom.

#### Relaying Push Notification Data to Loom

```swift
static func registeredForPushNotifications(withToken: Data)
```
Used to pass the push notification token from your app to Loom. This must be called from within your `UIApplicationDelegate`'s `application(:didRegisterForRemoteNotificationsWithDeviceToken:)`.

```swift
static func failedToRegisterForPushNotifications(withError: Error)
```
Used to pass the push notification token registration errors from your app to Loom. This must be called from within your `UIApplicationDelegate`'s `application(:didFailToRegisterForRemoteNotificationsWithError:)`.

```swift
static func receivedRemoteNotification(userInfo: [AnyHashable : Any], fetchCompletionHandler: @escaping (UIBackgroundFetchResult) -> Void)
```
Used to pass received push notifications from your app to Loom. Any non-Loom silent push notifications are ignored by Loom. This must be called from within your `UIApplicationDelegate`'s `application(:didReceiveRemoteNotification:fetchCompletionHandler:)`.

#### Tracking Custom Events in Loom

```swift
static func track(event: String, date: Date? = default)
```
Use this method to track your own custom app events in Loom. Loom will learn user behaviors around these events and generate feature data for them. Parameters are your custom event name and the event timestamp, defaulting to the current time if no timestamp is provided.

#### Setting Loom Metadata

```swift
static func set(userName: String)
```
Associate a user name with a Loom user. This name will be visible in your Textile feature data.
_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).
