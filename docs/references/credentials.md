```swift
struct Credentials
```

Holds references to your Textile client id and secret, used to authorize Loom.
_____________________________

#### Creating Credentials

```swift
init(clientId: String, clientSecret: String)
```
Create a `Credentials` instance with the client id and secret associated with your Textile client.

#### Properties

```swift
let clientId: String
```
Returns the client id that the `Credentials` were created with.

```swift
let clientSecret: String
```
Returns the client secret that the `Credentials` were created with.

_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).
