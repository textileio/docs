## Terms of Service

The websites, textile.io and textile.io (collectively referred to as the "site"), are owned and operated by We Are Set, Inc. ("Textile", "we" or “us”). By using the site, services provided on the site, or data features we make available to you through the services (collectively, "Services"), you agree to be bound by the following Terms of Service, as updated from time to time (collectively, the "Terms"). Please read them carefully. If you don’t agree to these Terms, you may not use the Services.

#### Signing up

In order to use most Services, you must register for a Textile account. When you use our software development kits (SDKs) you must include one of your account's unique account keys. Similarly, when you use our application program interfaces (APIs), each request to an API must include one of your account's unique API keys.

Please carefully guard the security of your account and monitor use of your API keys. You are responsible for all use of the Services under your account, whether or not authorized, including any use of your API keys. At our discretion, we may make limited exceptions to this policy for unauthorized use of your account if you notify us of the problem promptly.

You must be 13 years or older to use the Services. By registering as a user or providing personal information on the site, you represent that you are at least 13 years old.

If you are entering into this agreement on behalf of your company or another legal entity, you represent that you have the authority to bind that entity to these Terms, in which case "you" will mean the entity you represent.

#### Our services

Subject to these Terms, we grant you a non-exclusive, non-transferable, non-sublicensable, revocable license and right to:

* Use the Services to develop online services and online, desktop, or mobile applications; and
* Make the Services available to end users in connection with their use of your publicly available online services and publicly available online, desktop, or mobile applications.

You may only use the Services for non-commercial purposes, for commercial applications publicly accessible without charge, and for commercial mobile applications.

You may not use the Services in connection with any application or service that sells data or resells outputs of the Services.

If you want to make use of the Services outside this description--such as in non-public applications or data reselling--contact us about an Enterprise plan.

##### Firehose SDK

To use Textile on mobile, your app must use the Firehose SDK as its exclusive means of accessing Textile Services supported by the SDK. At any given time, the Textile SDK that you use must be the version that has been released within the immediately preceding 12 months (unless no such update has been released). The SDK you use will send the complete set of subscribed Features to us, which we may use periodically check quality, provide information back to you about how your app is being used, and for other purposes consistent with our privacy policy.

For all mobile applications that use Textile Services, you must:

* Limit use of your app to users at least 13 years of age or disable information sharing for users younger than 13.
* Obtain your users' affirmative express consent before you access their location (the automatic notifications in iOS and Android suffice).
* Disclose that data will be shared with Textile. Notification in your privacy policy is the minimum permitted.
* Allow users to opt out of location data sharing.
* Comply with any additional local requirements related to information access or sharing.
* Not interfere with or limit the data that the Firehose SDK sends to us, whether by modifying the SDK or by other means, except as otherwise required by the Terms.
* Not attempt to identify specific individuals, vehicles, or entities with which Feature data that we provide to you via the developer dashboard is associated (e.g. by re-identifying data).

##### Developer Dashboard

If you use the data export functions and APIs provided in the Dashboard, you must (1) only use such features internally, (2) not resell, sublicense or share such data with any third party, and (3) only use such data in compliance with the Firehose SDK terms set forth above.

#### Unlawful and other unauthorized uses

You agree to comply with all applicable laws, regulations, and third party agreements in your use of the Services.

You may not use the Services in any manner that could damage or overburden the Services or interfere with any other party's use of the Services.

You may not use the Services to:

1. Disseminate material that is abusive, obscene, pornographic, defamatory, harassing, grossly offensive, vulgar, threatening, or malicious;
2. Aid or implementing practices violating basic human rights or civil liberties. For the avoidance of doubt, you may not use the Services to assist in the creation of databases of identifying information for any government to abrogate any human rights, civil rights, or civil liberties of individuals on the basis of race, gender or gender identity, sexual orientation, religion, or national origin;
3. Disseminate or store material that infringes the copyright, trademark, patent, trade secret, or other intellectual property right of any person;
4. Create a false identity or otherwise attempting to mislead others as to the identity or origin of any communication;
5. Export, re-export, or permit downloading of any content in violation of any export or import law, regulation, or restriction of the United States and its agencies or authorities, or without all required approvals, licenses, or exemptions;
6. Interfere with or attempt to gain unauthorized access to any computer network;
7. Host with, transmit to or provide to us any information that is subject to specific government regulation, including, without limitation, Protected Health Information (as defined in the U.S. Health Insurance Portability and Accountability Act, as amended), financial information (as regulated by the U.S. Financial Services Modernization Act, as amended), consumer reports and consumer-reporting information (as regulated by the U.S. Fair Credit Reporting Act, as amended) and information subject to export control or economic sanction laws;
8. Operate dangerous businesses such as emergency services, where the use or failure of the Services could lead to death, personal injury or significant property damage;
9. Transmit viruses, trojan horses, or any other malicious code or program; or
10. Engage in any other activity reasonably deemed by Textile to be in conflict with the spirit or intent of these Terms.

You may not modify, create derivative works from, reverse engineer, or attempt to derive any source code from the site’s software, except as expressly permitted by a written license from us. Further, unless expressly prohibited under applicable law, you may not use the Services to develop, test, validate and/or improve any service or dataset that is a substitute for, or substantially similar to, the Services (including any portion thereof).

#### End users and notification

You may not allow your end users or other third parties to use the Services in any way that would be a violation of these Terms if done by you, and you agree to take reasonable efforts to prevent such use. You agree to promptly notify Textile in writing if you become aware of any misappropriation or unauthorized use of the Services.

#### Charges and payment

These Services require payment. We may charge your credit card on an ongoing basis in advance of providing Services or as needed for prepayments for your subscription fee, any applicable sales taxes, and any other charges you may incur in connection with your use of Textile Services. The subscription fee is billed in full on the first day of each billing period, unless and until you cancel your subscription.

Sometimes even you don't know how popular your apps are going to be. If your account exceeds the monthly active users allowed by your plan, we may charge you overage fees at the end of your billing period. Overage rates vary by plan. You agree to pay overage fees for all usage including requests that result from unexpected traffic.

We are not responsible for any bank fees, interest charges, finance charges, over draft charges, or other fees resulting from charges billed by Textile. Currency exchange settlements will be based on agreements between you and the provider of your credit card.

All charges are non-refundable unless expressly stated otherwise, or otherwise provided by applicable law.

#### Ownership

##### Your Content

You retain ownership of all data that you contribute to the Services via the Firehose SDK and the Developer Dashboard, excluding any content that you receive 3rd Party Integrations ("Your Content").

Limited to the purpose of hosting Your Content so that we can provide the Services to you, you hereby grant Textile a non-exclusive, worldwide, royalty-free, fully paid-up, transferable and sublicensable right and license to use, copy, cache, publish, display, distribute, modify, create derivative works, and store Your Content and to allow others to do so. This right and license enables Textile to host and mirror your content on its distributed platform. You warrant, represent, and agree that you have the right to grant Textile these rights.

On termination of your account, Textile will make reasonable efforts to promptly remove from the site and cease use of Your Content; however, you recognize and agree that caching of or references to the content may not be immediately removed.

##### Our content and third party content

Other than Your Content, all content displayed on the site or accessible through the Services, including text, images, software or source code, are the property of Textile and/or third parties and are protected by United States and international intellectual property laws. Logos and product names appearing on or in connection with the Services are proprietary to Textile or our licensors. You may not remove any proprietary notices or product identification labels from the Services.

##### Feedback

You agree that we may freely exploit and make available any and all feedback, suggestions, ideas, enhancement requests, recommendations or other information you provide relating to the Services.

<br />

<br />

#### Publicity

We're proud to have you as a customer. During the term of this agreement, you hereby grant us a worldwide, non-exclusive, royalty-free, fully paid-up, transferable and sublicensable license to use your trademarks, service marks, and logos for the purpose of identifying you as a Textile customer to promote and market our services. But if you prefer we not use your logo or name in a particular way, just let us know, and we will respect that.

#### Account cancellation or suspension

We don't want you to leave, but you may cancel at any time. However, we do not give pro-rated refunds for unused time if you cancel during the middle of a billing cycle.

If you breach any of these Terms, we may immediately without notice cancel or suspend your account and the limited license granted to you hereunder automatically terminates, without notice to you. Upon termination of the limited license, you agree to immediately destroy any materials downloaded from the Services. In addition, Textile may cancel or suspend your account for any reason by providing you 30 days' advance notice.

Upon cancellation or suspension, your right to use the Services will stop immediately. You may not have access to data that you stored on the site after we cancel or suspend your account. You are responsible for backing up data that you use with the Services. If we cancel your account in its entirety without cause, we will refund to you on a pro-rata basis the amount of your payment corresponding to the portion of your service remaining right before we cancelled your account.

#### Changes to services or terms

We may modify these Terms and other terms related to your use of the Services (like our privacy policy) from time to time, by posting the changed terms on the site. All changes will be effective immediately upon posting to the site unless they specify a later date. Changes will not apply retroactively. Please check these Terms periodically for changes - your continued use of the Services after new terms become effective constitutes your binding acceptance of the new terms.

We may change the features and functions of the Services, including APIs. It is your responsibility to ensure that calls or requests you makes to the Services are compatible with then-current Textile APIs. We attempt to avoid changes to our APIs that are not backwards compatible, but such changes may occasionally be required. If that happens, we will use reasonable efforts to notify you prior to deploying the changes.

#### Indemnification

You agree to indemnify and hold harmless Textile and its subsidiaries, affiliates, officers, agents, partners, and employees from any claim or demand, including reasonable attorneys' fees, arising out of:

* Your use of the Services;
* Your violation of these Terms;
* Your end users’ use of the Services in or through an application or service you provide;
* Content you or your end users submit, post to, extracts from, or transmit through the Services.

#### Disclaimers

“As is," "as available" and "with all faults." YOU EXPRESSLY AGREE THAT THE USE OF THE SERVICES IS AT YOUR SOLE RISK. THE SITE AND ITS SOFTWARE, SERVICES, AND OTHER CONTENT, INCLUDING ANY THIRD-PARTY SOFTWARE, SERVICES, MEDIA, OR OTHER CONTENT MADE AVAILABLE IN CONJUNCTION WITH OR THROUGH THE SITE, ARE PROVIDED ON AN "AS IS", "AS AVAILABLE", "WITH ALL FAULTS" BASIS AND WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

No warranties. TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, TEXTILE DISCLAIMS ALL WARRANTIES, STATUTORY, EXPRESS OR IMPLIED, INCLUDING IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT OF PROPRIETARY RIGHTS. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM TEXTILE OR THROUGH THE SITE, WILL CREATE ANY WARRANTY NOT EXPRESSLY STATED HEREIN.

Website operation. TEXTILE DOES NOT WARRANT THAT THE SERVICES, INCLUDING ANY SOFTWARE, SERVICES, OR CONTENT OFFERED ON OR THROUGH THE SITE OR ANY THIRD PARTY SITES REFERRED TO ON OR BY THE SITE WILL BE UNINTERRUPTED, OR FREE OF ERRORS, VIRUSES, OR OTHER HARMFUL COMPONENTS AND DOES NOT WARRANT THAT ANY OF THE FOREGOING WILL BE CORRECTED.

Accuracy. TEXTILE DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE SERVICES OR ANY THIRD PARTY SITES REFERRED TO ON OR BY THE SITE IN TERMS OF CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE.

Harm to your computer. YOU UNDERSTAND AND AGREE THAT YOU USE, ACCESS, DOWNLOAD, OR OTHERWISE OBTAIN SOFTWARE, SERVICES, OR CONTENT THROUGH THE SITE OR ANY THIRD PARTY SITES REFERRED TO ON OR BY THE SITE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR PROPERTY (INCLUDING YOUR COMPUTER SYSTEM) OR LOSS OF DATA THAT RESULTS FROM SUCH DOWNLOAD OR USE.

Jurisdiction. CERTAIN JURISDICTIONS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF YOU RESIDE IN SUCH A JURISDICTION, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU, AND YOU MAY HAVE ADDITIONAL RIGHTS. THE LIMITATIONS OR EXCLUSIONS OF WARRANTIES, REMEDIES, OR LIABILITY CONTAINED IN THESE TERMS APPLY TO YOU TO THE FULLEST EXTENT SUCH LIMITATIONS OR EXCLUSIONS ARE PERMITTED UNDER THE LAWS OF THE JURISDICTION IN WHICH YOU ARE LOCATED.

#### Limitation of liability

Limitation of liability. UNDER NO CIRCUMSTANCES, AND UNDER NO LEGAL THEORY, INCLUDING NEGLIGENCE, SHALL TEXTILE OR ITS AFFILIATES, CONTRACTORS, EMPLOYEES, AGENTS, OR THIRD PARTY PARTNERS OR SUPPLIERS, BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, CONSEQUENTIAL, OR EXEMPLARY DAMAGES (INCLUDING LOSS OF PROFITS, DATA, OR USE OR COST OF COVER) ARISING OUT OF OR RELATING TO THESE TERMS OR THAT RESULT FROM YOUR USE OR THE INABILITY TO USE THE SERVICES OR THE SITE, INCLUDING SOFTWARE, SERVICES, CONTENT, USER SUBMISSIONS, OR ANY THIRD PARTY SITES REFERRED TO ON OR BY THE SITE, EVEN IF TEXTILE OR A TEXTILE AUTHORIZED REPRESENTATIVE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

Limitation of damages. IN NO EVENT SHALL THE TOTAL LIABILITY OF TEXTILE OR ITS AFFILIATES, CONTRACTORS, EMPLOYEES, AGENTS, OR THIRD PARTY PARTNERS, LICENSORS, OR SUPPLIERS TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF ACTION ARISING OUT OF OR RELATING TO THESE TERMS THE SERVICES OR YOUR USE OF THE SITE (WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), WARRANTY, OR OTHERWISE) EXCEED THE GREATER OF ONE HUNDRED DOLLARS ($100 USD) OR FEES PAID OR PAYABLE TO TEXTILE IN THE TWELVE MONTHS PERIOD PRIOR TO THE DATE ON WHICH THE DAMAGE OCCURRED.

Claim period. YOU AND TEXTILE AGREE THAT ANY CAUSE OF ACTION ARISING OUT OF THESE TERMS OR RELATED TO TEXTILE MUST COMMENCE WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES. OTHERWISE, SUCH CAUSE OF ACTION IS PERMANENTLY BARRED.

#### Additional terms

You agree to keep the contact information associated with your Textile account current and complete.

You may not encourage others to violate these Terms, including by selling products or services that would violate these Terms if the products or services are used in their intended manner.

You may not create multiple accounts for the purpose of increasing the free services that you receive under a Textile plan. Except as provided in these Terms, the license and right to use the Services is granted only to you and may not be transferred to anyone without the prior written consent of Textile. These Terms shall benefit Textile and its successors and assignees.

You shall not assign these Terms or any right, interest or benefit hereunder without the prior written consent of Textile, which may be withheld for any reason or no reason at all. Textile may assign (i) these Terms to an affiliate, (ii) these Terms or any right, interest or benefit hereunder to a third party in connection with a collection proceeding against you, and (iii) these Terms in their entirety to its successor in interest pursuant to a merger, acquisition, corporate reorganization, or sale of all or substantially all of that party’s business or assets to which these Terms relate.

These Terms are governed by and construed in accordance with the laws of California, without giving effect to any principles of conflicts of law. Any action arising out of or relating to these Terms must be filed in the state or federal courts for San Francisco County, California, USA, and you hereby consent and submit to the exclusive personal jurisdiction and venue of these courts for the purposes of litigating any such action.

A provision of these Terms may be waived only by a written instrument executed by the party entitled to the benefit of such provision. The failure of Textile to exercise or enforce any right or provision of these Terms will not constitute a waiver of such right or provision. Textile reserves all rights not expressly granted to you.

If any provision of these Terms is held to be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from these Terms and shall not affect the validity and enforceability of any remaining provisions. Headings are for convenience only and have no legal or contractual effect.

You agree that no joint venture, partnership, employment, or agency relationship exists between you and Textile as a result of these Terms or your use of the Services. You further acknowledge no confidential, fiduciary, contractually implied, or other relationship is created between you and Textile other than pursuant to these Terms.
