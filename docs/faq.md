## Permissions

#### What permissions and capabilities does the SDK require?

The Loom SDK only requires ability to send remote updates in Background mode. All other permissions are optional. However, several permission types, if granted, will improve the performance and capabilities of the Loom SDK. Those include background location and motion for example.  

#### Will location permission create a blue bar in iOS 11?

No matter what permissions your users grant your app, the Loom SDK will never cause the persistent blue bar reported by some iOS 11 beta testers. Our SDK is designed to run efficiently in the background and will not ever effect your application's expected appearance.

_____________________________

* If you **need help**, ping us on our [Slack community](https://slack.textile.io/).
* If **found a bug**, or **have a feature request**, add an Issue on the [Loom SDK Repo](https://gitlab.com/weareset/Loom-SDK/issues).
* Follow us on [Twitter](https://twitter.com/textile01).